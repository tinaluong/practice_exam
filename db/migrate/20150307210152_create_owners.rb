class CreateOwners < ActiveRecord::Migration
  def change
    create_table :owners do |t|
      t.string :name
      t.integer :address_id
      t.integer :car_id
      t.timestamps null: false
    end
  end
end
