json.array!(@cars) do |car|
  json.extract! car, :id, :owner_id, :make, :model, :vin_number
  json.url car_url(car, format: :json)
end
